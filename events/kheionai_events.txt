# events for kheionai death winds & warding
namespace = kheionai

# death wind event
country_event = {
	id = kheionai.1
	title = kheionai.1.t
	desc = kheionai.1.d
	picture = WOUNDED_SOLDIERS_eventPicture

	trigger = {
		NOT = { has_country_modifier = recent_kaydhano_wind }
		any_owned_province = {
			region = alecand_region
		}
	}

	mean_time_to_happen = {
		days = 3650
	}

	immediate = {
		hidden_effect = {
			add_country_modifier = {
				name = recent_kaydhano_wind
				duration = 3650
				hidden = yes
			}
			every_owned_province = {
				limit = {
					region = alecand_region
					NOT = { has_province_modifier = kheionai_warded_city}
				}
				if = {
					limit = {
						province_group = east_alecand
					}
					add_province_modifier = {
						name = deadly_kaydhano_winds
						duration = 365
					}
					add_devastation = 5
				}
				if = {
					limit = {
						NOT = { province_group = east_alecand }
					}
					add_province_modifier = {
						name = lesser_kaydhano_winds
						duration = 365
					}
					add_devastation = 2
				}	
			}
			every_neighbor_country = {
				country_event = {
					id = kheionai.1
				}
			}
		}
	}

	option = {
		name = kheionai.1.a
		custom_tooltip = deathwind_tooltip
	}
}


#generate warding on forts
country_event = {
	id = kheionai.2
	title = kheionai.2.t
	desc = kheionai.2.d
	picture = BIG_BOOK_eventPicture
	
	is_triggered_only = yes

	trigger = {
		any_owned_province = {
			region = alecand_region
			NOT = { has_province_modifier = kheionai_warded_city }
			fort_level = 1
		}
	}

	immediate = {
		hidden_effect = {
			every_owned_province = {
				limit = {
					region = alecand_region
					NOT = { has_province_modifier = kheionai_warded_city }
					fort_level = 1
				}
				if = {
					limit = {
						owner = { #prevent eltibhari from creating warded cities
							NOT = { culture_group = eltibhari_ruinborn_elf } 
						}
					}
					add_permanent_province_modifier = {
						name = kheionai_warded_city
						duration = -1
					}
				}
				if = {
					limit = {
						has_province_modifier = lack_of_ward
					}
					remove_province_modifier = lack_of_ward
				}
			}
		}
	}

	option = {
		name = kheionai.1.a
		tooltip = {
			every_owned_province = {
				limit = {
					region = alecand_region
					NOT = { has_province_modifier = kheionai_warded_city }
					fort_level = 1
				}
				if = {
					limit = {
						owner = { #prevent eltibhari from creating warded cities
							NOT = { culture_group = eltibhari_ruinborn_elf } 
						}
					}
					add_permanent_province_modifier = {
						name = kheionai_warded_city
						duration = -1
					}
				}
				if = {
					limit = {
						has_province_modifier = lack_of_ward
					}
					remove_province_modifier = lack_of_ward
				}
			}
		}
	}
}


# set unwarded provinces -- TO DO set this in history/provinces as well
country_event = {
	id = kheionai.3
	title = kheionai.3.t
	desc = kheionai.3.d
	picture = WOUNDED_SOLDIERS_eventPicture

	is_triggered_only = yes
	hidden = yes

	trigger = {
		any_owned_province = {
			region = alecand_region
			OR = {
				NOT = { has_province_modifier = lack_of_ward }
				NOT = { has_province_modifier = kheionai_warded_city }
			}
			
		}
	}

	immediate = {
		every_owned_province = {
			limit = {
				region = alecand_region
				NOT = { has_province_modifier = kheionai_warded_city }
				NOT = { has_province_modifier = lack_of_ward }
			}
			if = {
				limit = {
					province_group = east_alecand
				}
				add_permanent_province_modifier = {
					name = lack_of_ward
					duration = -1
				}
			}
			if = {
				limit = {
					NOT = { province_group = east_alecand }
				}
				add_permanent_province_modifier = {
					name = lesser_lack_of_ward
					duration = -1
				}
			}
		}
	}

	option = {
		name = kheionai.3.a
	}
}

# remove lack of ward on newly warded cities
country_event = {
	id = kheionai.4
	title = kheionai.4.t
	desc = kheionai.4.d
	picture = WOUNDED_SOLDIERS_eventPicture
	
	is_triggered_only = yes
	hidden = yes

	trigger = {
		any_owned_province = {
			region = alecand_region
			AND = {
				has_province_modifier = lack_of_ward
				has_province_modifier = kheionai_warded_city
			}
		}
	}

	immediate = {
		every_owned_province = {
			limit = {
				region = alecand_region
				has_province_modifier = kheionai_warded_city
				has_province_modifier = lack_of_ward
			}
			if = {
				limit = {
					province_group = east_alecand
				}
				remove_province_modifier = lack_of_ward
			}
			if = {
				limit = {
					NOT = { province_group = east_alecand }
				}
				remove_province_modifier = lesser_lack_of_ward
			}
		}
	}

	option = {
		name = kheionai.4.a
	}
}

#random flavour events

#enemy damages warding
country_event = {
	id = kheionai.5
	title = kheionai.5.t
	desc = kheionai.5.d
	picture = ASSASSINATION_eventPicture

	trigger = {
		any_owned_province = {
			region = alecand_region
			has_province_modifier = kheionai_warded_city
		}
		any_enemy_country = {
			culture_group = kheionai_ruinborn_elf
		}
	}

	mean_time_to_happen = {
		years = 10
	}

	immediate = {
		hidden_effect = {
			random_owned_province = {
				limit = {
					region = alecand_region
					has_province_modifier = kheionai_warded_city
				}
				add_province_modifier = {
					name = damaged_warding
					duration = 365
				}
				add_devastation = 5
			}
			random_enemy_country = {
				save_event_target_as = enemy_country
			}
		}
	}

	option = { #bastards
		name = kheionai.5.a
		tooltip = {
			random_owned_province = {
				limit = {
					has_province_modifier = damaged_warding
				}
				add_province_modifier = {
					name = damaged_warding
					duration = 365
				}
				add_devastation = 5
			}
		}
		add_opinion ={
			who = event_target:enemy_country
			modifier = damaged_warding
		}
	}
}